FROM openjdk:latest

ARG JAR_FILE

COPY ${JAR_FILE} vehicles-service.jar