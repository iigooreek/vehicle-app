package com.example.web.controller;

import com.example.data.entity.Manufacturer;
import com.example.service.ManufacturerService;
import com.example.web.validation.ManufacturerValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manufacturers")
@Api(value = "Endpoint to manage manufacturers",
        description = "Operations pertaining to manufacturer lifecycle")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @ApiOperation(value = "Create new Manufacturer")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created manufacturer"),
            @ApiResponse(code = 400, message = "Validation error on some fields"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity<Void> create(@RequestBody Manufacturer manufacturer) {
        ManufacturerValidator.validate(manufacturer);
        manufacturerService.save(manufacturer);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "View a list of all manufacturers")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Manufacturer>> getAll() {
        return new ResponseEntity<>(manufacturerService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long manufacturerId) {
        manufacturerService.deleteById(manufacturerId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
