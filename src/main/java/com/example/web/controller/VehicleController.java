package com.example.web.controller;

import com.example.data.entity.Vehicle;
import com.example.service.VehicleService;
import com.example.web.dto.VehicleDto;
import com.example.web.validation.VehicleValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "manufactures/{id}")
    public ResponseEntity<Void> create(@PathVariable("id") Long manufacturersId, @RequestBody Vehicle vehicle){
        VehicleValidator.validate(vehicle);
        vehicleService.createVehicleForManufacturer(manufacturersId, vehicle);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> getAll() {
        List<VehicleDto> collect = vehicleService.getAllVehicles().stream().map(VehicleDto::from).collect(Collectors.toList());
        return new ResponseEntity<>(collect, HttpStatus.OK);
    }
}
