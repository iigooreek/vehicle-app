package com.example.web.validation;

import com.example.data.entity.Vehicle;
import com.example.exception.ManufacturerValidationException;
import com.example.exception.VehicleValidationException;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VehicleValidator {

    private static final String EMPTY_PROPERTY_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must be provided";
    private static final String REGEX_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must match these parameters: '%s'";
    private static final String VALID_VIN_REGEX = "[a-zA-Z0-9]{9}[a-zA-Z0-9-]{2}[0-9]{6}";
    private static final String VALID_NUMBER_REGEX = "^([0-9]*[1-9][0-9]*(\\.[0-9]+)?|[0]+\\.[0-9]*[1-9][0-9]*)$";
    public static void validate(Vehicle vehicle) throws ManufacturerValidationException {
        validateNotEmptyProperty(vehicle.getEngineCapacity(), "engineCapacity");
        validateNotEmptyProperty(vehicle.getSeats(), "seats");
        validateNotEmptyProperty(vehicle.getWheels(), "wheels");
        validateNotEmptyProperty(vehicle.getVinNumber(), "VIN");
        validateNotEmptyProperty(vehicle.getVehicleMass(), "mass");
        validateNotEmptyProperty(vehicle.getColor(), "color");
        validateNotEmptyProperty(vehicle.getModel(), "model");
        validateVinWithRegularExpression(vehicle.getVinNumber(), VALID_VIN_REGEX, "VIN",
                "Invalid VIN number");
        validateWithRegularExpression(vehicle.getEngineCapacity(), VALID_NUMBER_REGEX, "engineCapacity",
                "Engine capacity must be above 0");
        validateWithRegularExpression(vehicle.getSeats(), VALID_NUMBER_REGEX, "seats",
                "Engine capacity must be above 0");
        validateWithRegularExpression(vehicle.getWheels(), VALID_NUMBER_REGEX, "wheels",
                "Engine capacity must be above 0");
        validateWithRegularExpression(vehicle.getVehicleMass(), VALID_NUMBER_REGEX, "mass",
                "Engine capacity must be above 0");
    }
    private static void validateNotEmptyProperty(Object value, String propertyName) {
        if (value == null || StringUtils.isEmpty(value)) {
            throw new VehicleValidationException(String.format(EMPTY_PROPERTY_EXCEPTION_MESSAGE, propertyName));
        }
    }
    private static void validateWithRegularExpression(Object value,
                                                      String regex,
                                                      String propertyName,
                                                      String exceptionMessage) {
        Matcher matcher = Pattern.compile(regex).matcher(String.valueOf(value));
        if (!matcher.matches()) {
            throw new ManufacturerValidationException(String.format(REGEX_EXCEPTION_MESSAGE, propertyName, exceptionMessage));
        }
    }

    private static void validateVinWithRegularExpression(Object value,
                                                         String regex,
                                                         String propertyName,
                                                         String exceptionMessage) {
        Matcher matcher = Pattern.compile(regex).matcher(String.valueOf(value));
        if (!matcher.matches()) {
            throw new VehicleValidationException(String.format(REGEX_EXCEPTION_MESSAGE, propertyName, exceptionMessage));
        }
    }

}
