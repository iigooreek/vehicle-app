package com.example.data.repository;

import com.example.data.entity.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {

    Optional<Vehicle> findVehicleByVinNumber(String vinNumber);

}
