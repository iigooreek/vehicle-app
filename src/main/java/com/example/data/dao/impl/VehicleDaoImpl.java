package com.example.data.dao.impl;

import com.example.data.dao.VehicleDao;
import com.example.data.entity.Vehicle;
import com.example.data.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VehicleDaoImpl implements VehicleDao {

    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleDaoImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List findAll() {
        return (ArrayList) this.vehicleRepository.findAll();
    }
    public Vehicle save(Vehicle vehicle) {
        this.vehicleRepository.save(vehicle);
        return vehicle;
    }
}
