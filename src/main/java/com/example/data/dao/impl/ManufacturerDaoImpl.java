package com.example.data.dao.impl;

import com.example.data.dao.ManufacturerDao;
import com.example.data.entity.Manufacturer;
import com.example.data.repository.ManufacturerRepository;
import com.example.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ManufacturerDaoImpl implements ManufacturerDao {

    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerDaoImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public Manufacturer findById(Long id) {
        return this.manufacturerRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Manufacturer with id '%s' not found", id)));
    }

    @Override
    public void deleteById(Long id) {
        this.manufacturerRepository.deleteById(id);
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        this.manufacturerRepository.save(manufacturer);
        return manufacturer;
    }

    @Override
    public List<Manufacturer> findAll() {
        return (ArrayList) this.manufacturerRepository.findAll();
    }
}
