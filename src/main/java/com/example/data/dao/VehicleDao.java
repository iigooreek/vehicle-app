package com.example.data.dao;

import com.example.data.entity.Vehicle;

import java.util.List;

public interface VehicleDao {
    List<Vehicle> findAll();
    Vehicle save(Vehicle vehicle);
}
