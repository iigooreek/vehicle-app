package com.example.data.dao;

import com.example.data.entity.User;

import java.util.Optional;

public interface UserDao {

    Optional<User> findByUsernameAndPassword(String username, String password);

}
