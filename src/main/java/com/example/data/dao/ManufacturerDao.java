package com.example.data.dao;

import com.example.data.entity.Manufacturer;

import java.util.List;

public interface ManufacturerDao {
    Manufacturer findById(Long id);
    void deleteById(Long id);
    Manufacturer save(Manufacturer manufacturer);
    List<Manufacturer> findAll();
}
