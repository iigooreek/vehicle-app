package com.example.service;

import com.example.data.entity.User;

import java.util.Optional;

public interface UserService {
    Optional<User> getByUsernameAndPassword(String username, String password);
}
