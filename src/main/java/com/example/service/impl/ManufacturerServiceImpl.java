package com.example.service.impl;

import com.example.data.dao.ManufacturerDao;
import com.example.data.entity.Manufacturer;
import com.example.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ManufacturerServiceImpl implements ManufacturerService {
    private final ManufacturerDao manufacturerDao;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerDao manufacturerDao) {
        this.manufacturerDao = manufacturerDao;
    }

    @Override
    public Manufacturer findById(Long id) {
        return manufacturerDao.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        manufacturerDao.deleteById(id);
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        return manufacturerDao.save(manufacturer);
    }

    @Override
    public List<Manufacturer> findAll() {
        return manufacturerDao.findAll();
    }
}
