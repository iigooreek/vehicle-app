create table user
(
    id bigint not null primary key auto_increment,
    username varchar(255) not null,
    password varchar(255) not null,
    role varchar(255) not null
);

insert into user (username, password, role) value ('admin', 'admin', 'admin');
insert into user (username, password, role) value ('user', 'password', 'user');