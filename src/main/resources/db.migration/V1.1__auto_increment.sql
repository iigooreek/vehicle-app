SET FOREIGN_KEY_CHECKS = 0;
alter table manufacturer
    modify column id bigint not null auto_increment;
alter table vehicle
    modify column id bigint not null auto_increment;
SET FOREIGN_KEY_CHECKS = 1;