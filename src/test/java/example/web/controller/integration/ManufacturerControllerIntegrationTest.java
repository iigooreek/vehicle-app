package example.web.controller.integration;//package com.example.web.controller.integration;
//
//import com.example.data.dao.ManufacturerDao;
//import com.example.data.entity.Manufacturer;
//import com.example.service.ManufacturerService;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.mock.mockito.SpyBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.jdbc.Sql;
//import org.springframework.test.context.jdbc.SqlGroup;
//
//import java.util.List;
//import java.util.Objects;
//import java.util.Set;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.Matchers.is;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.mockito.Mockito.verify;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD) //clears all after each method
//public class ManufacturerControllerIntegrationTest extends IntegrationBaseTest {
//
//    @Autowired
//    private ManufacturerDao manufacturerDao;
//
//    @Autowired
//    @SpyBean
//    private ManufacturerService manufacturerService;
//
//
//    @Test
//    void whenEmptyManufacturersListShouldRespondOkTest() throws Exception {
//        mockMvc.perform(get("/manufacturers")
//                .contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$", hasSize(0)))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    @SqlGroup({
//            @Sql(statements = "INSERT INTO manufacturer" +
//                    " (id, address, car_model_name, company_name, foundation_year)" +
//                    " VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
//    })
//    @DisplayName("should return 'manufacturer with id 1 and HTTP 200'")
//    void whenListOfManufacturersListShouldRespondOkAndReturnListTest() throws Exception {
//        mockMvc.perform(get("/manufacturers")
//                .contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].address", is("Berlin")))
//                .andExpect(jsonPath("$[0].companyName", is("BMW")))
//                .andExpect(jsonPath("$[0].carModelName", is("BMW")))
//                .andExpect(jsonPath("$[0].foundationYear", is(1904)))
//                .andExpect(jsonPath("$[0].id", is(1)));
//    }
//
//    @Test
//    @DisplayName("should return 'HTTP 201'")
//    void whenCreateManufacturerShouldReturnCreatedTest() throws Exception {
//        Manufacturer expected = Manufacturer.builder()
//                .address("Berlin 191")
//                .companyName("Mercedes")
//                .carModelName("Mercedes")
//                .foundationYear(1194)
//                .id(1L)
//                .vehicles(Set.of())
//                .build();
//
//        String manufacturerJson = "{\"companyName\":\"Mercedes\",\"carModelName\":\"Mercedes\",\"address\":\"Berlin 191\",\"foundationYear\":1194}";
//
//        mockMvc.perform(post("/manufacturers")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content(manufacturerJson))
//                .andExpect(status().isCreated());
//
//        verify(manufacturerService).save(expected);
//
//        List<Manufacturer> allManufacturers = manufacturerDao.findAll();
//
//        assertEquals(1, allManufacturers.size());
//
//        Manufacturer actual = allManufacturers.get(0);
//
//        assertEquals(expected.getCompanyName(), actual.getCompanyName());
//        assertEquals(expected.getAddress(), actual.getAddress());
//        assertEquals(expected.getCarModelName(), actual.getCarModelName());
//        assertEquals(expected.getFoundationYear(), actual.getFoundationYear());
//        assertEquals(expected.getId(), actual.getId());
//    }
//
//}
