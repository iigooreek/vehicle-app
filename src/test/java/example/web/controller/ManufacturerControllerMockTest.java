//package example.web.controller;
//
//import com.example.data.entity.Manufacturer;
//import com.example.service.ManufacturerService;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//
//import java.util.List;
//import java.util.Set;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.Matchers.is;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//public class ManufacturerControllerMockTest extends ControllerBaseTest {
//
//    @MockBean
//    private ManufacturerService manufacturerService;
//
//    @Test
//    void whenEmptyManufacturesListShouldResponseOkTest() throws Exception {
//
//        //when
//        when(manufacturerService.findAll()).thenReturn(List.of());
//
//        mockMvc.perform(get("/manufacturers")
//                .contentType(MediaType.APPLICATION_JSON_UTF8))
//
//                .andExpect(jsonPath("$", hasSize(0)))
//
//                .andExpect(status().isOk());    //assert
//
//    }
//
//    @Test
//    void whenListOfManufacturersListShouldRespondOkAndReturnListTest() throws Exception {
//        Manufacturer manufacturer1 = Manufacturer.builder()
//                .address("Detroit")
//                .companyName("Ford")
//                .carModelName("Ford")
//                .foundationYear(1903)
//                .id(1l)
//                .build();
//
//        when(manufacturerService.findAll()).thenReturn(List.of(manufacturer1));
//
//        mockMvc.perform(get("/manufacturers")
//                .contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].address", is("Detroit")))
//                .andExpect(jsonPath("$[0].companyName", is("Ford")))
//                .andExpect(jsonPath("$[0].carModelName", is("Ford")))
//                .andExpect(jsonPath("$[0].foundationYear", is(1903)))
//                .andExpect(jsonPath("$[0].id", is(1)));
//    }
//
//    @Test
//    void whenCreateManufacturerShouldReturnCreatedTest() throws Exception {
//        Manufacturer manufacturer = Manufacturer.builder()
//                .address("Berlin 191")
//                .companyName("Mercedes")
//                .carModelName("Mercedes")
//                .foundationYear(1194)
//                .id(null)
//                .vehicles(Set.of())
//                .build();
//
//        String manufacturerJson = "{\"companyName\":\"Mercedes\",\"carModelName\":\"Mercedes\",\"address\":\"Berlin 191\",\"foundationYear\":1194}";
//
//        mockMvc.perform(post("/manufacturers")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content(manufacturerJson))
//                .andExpect(status().isCreated());
//
//        verify(manufacturerService).save(manufacturer);
//    }
//
//    @Test
//    void whenCreateManufacturerShouldThrowAnExceptionTest() throws Exception {
//        Manufacturer manufacturer = Manufacturer.builder()
//                .address("Berlin 191")
//                .companyName("Mercedes")
//                .carModelName("Mercedes")
//                .foundationYear(1194)
//                .id(null)
//                .vehicles(Set.of())
//                .build();
//        String manufacturerJson = "{\"companyName\":\"Mercedes\",\"carModelName\":\"Mercedes\",\"address\":\"Berlin 191\",\"foundationYear\":11194}";
//
//        mockMvc.perform(post("/manufacturers")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content(manufacturerJson))
//                .andExpect(status().isBadRequest())
//                .andExpect(jsonPath("$.message", is("Manufacturer field parameter 'foundationYear' must match these parameters: 'Year must consist of four digits'")))
//                .andExpect(jsonPath("$.error", is("com.example.exception.ManufacturerValidationException")));
//
//    }
//
//    @Test
//    void whenDeleteManufacturerShouldReturnHttpStatusOkTest() throws Exception {
//        mockMvc.perform(delete("/manufacturers/1"))
//                .andExpect(status().isOk());
//
//        verify(manufacturerService).deleteById(1L);
//    }
//
//}
